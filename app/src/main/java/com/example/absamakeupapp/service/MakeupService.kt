package com.example.absamakeupapp.service

import com.example.absamakeupapp.models.Makeup
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MakeupService {

    @GET("products.json")
    fun getAllMakeup(): Call<List<Makeup>>

    //query api url and create makeup list object, convert json to makeup object using gson.
    companion object{
        private var makeUpService: MakeupService? = null
        fun getInstance() : MakeupService{
            if(makeUpService == null){
                val retrofit = Retrofit.Builder()
                    .baseUrl("http://makeup-api.herokuapp.com/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                makeUpService = retrofit.create(MakeupService::class.java)
            }
            return makeUpService!!
        }
    }
}