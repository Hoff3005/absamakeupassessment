package com.example.absamakeupapp.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.absamakeupapp.R
import com.example.absamakeupapp.models.ListItem
import com.example.absamakeupapp.models.Makeup
import com.example.absamakeupapp.models.GeneralItem
import com.example.absamakeupapp.models.HeaderItem

class MakeupAdapter(private val onItemClicked: (id: Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var makeupList = mutableListOf<ListItem>()

    //set data set for makeup list for recycler view. notify of data change.
    @SuppressLint("NotifyDataSetChanged")
    fun setMakeupsList(makeups: List<ListItem>) {
        this.makeupList = makeups.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        //depending on list item type load different view holders, one for header and another for normal makeup item.
        when (viewType) {
            ListItem.TYPE_GENERAL -> {
                val v1: View = inflater.inflate(
                    R.layout.makeup_list_item, parent,
                    false
                )
                viewHolder = GeneralViewHolder(v1, onItemClicked)
            }
            ListItem.TYPE_HEADER -> {
                val v2: View = inflater.inflate(R.layout.makeup_list_item_header, parent, false)
                viewHolder = HeaderViewHolder(v2)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //set data to view items based on which view holder type is used.
        when (holder.itemViewType) {
            ListItem.TYPE_GENERAL -> {
                val makeupItem =  makeupList[position] as GeneralItem
                val makeup : Makeup = makeupItem.getItem()

                val generalViewHolder = holder as GeneralViewHolder
                generalViewHolder.makeupName.text = makeup.name?.trim()
                generalViewHolder.makeupProductType.text = makeup.product_type?.trim()
            }
            ListItem.TYPE_HEADER -> {
                val makeupItem =  makeupList[position] as HeaderItem
                val headerViewHolder: HeaderViewHolder = holder as HeaderViewHolder
                headerViewHolder.makeupHeader.text = makeupItem.getCategory().trim()
            }
        }
    }

    override fun getItemCount(): Int {
        return makeupList.size
    }

    //override view type with custom type created in ListItem.kt
    override fun getItemViewType(position: Int): Int {
        return makeupList[position].getType()
    }

    //trigger onclick of clicking on makeup items in recycler view.
    class GeneralViewHolder(
        ItemView: View,
        private val onItemClicked: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(ItemView), View.OnClickListener  {
        val makeupName: TextView = itemView.findViewById(R.id.itemMakeupNameTextView)
        val makeupProductType: TextView = itemView.findViewById(R.id.itemMakeupProductTypeTextView)
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = bindingAdapterPosition
            onItemClicked(position)
        }
    }

    //Header view holder, only one text item.
    class HeaderViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val makeupHeader: TextView = itemView.findViewById(R.id.makeupHeaderTextView)
    }
}