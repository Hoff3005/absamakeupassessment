package com.example.absamakeupapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.absamakeupapp.adapters.MakeupAdapter
import com.example.absamakeupapp.databinding.ActivityMakeUpMainBinding
import com.example.absamakeupapp.models.GeneralItem
import com.example.absamakeupapp.models.ListItem
import com.example.absamakeupapp.models.Makeup
import com.example.absamakeupapp.repo.MakeupRepository
import com.example.absamakeupapp.service.MakeupService
import com.example.absamakeupapp.viewmodel.MakeupViewModelFactory
import com.example.absamakeupapp.viewmodel.MakeupViewModel

class MakeUpMainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMakeUpMainBinding
    private lateinit var viewModel: MakeupViewModel
    private val retrofitService = MakeupService.getInstance()
    private val adapter = MakeupAdapter{position -> onListItemClick(position) }
    private var makeupListItems = listOf<ListItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_up_main)
        binding = ActivityMakeUpMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Set action bar title
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.actionbar_makup_list_text)

        //Set recycler view adapter.
        binding.makeupRecyclerView.adapter = adapter

        //create view model factory and fetch makeup list from api.
        viewModel = ViewModelProvider(this, MakeupViewModelFactory(MakeupRepository(retrofitService))).get(MakeupViewModel::class.java)
        viewModel.getAllMakeUps()

        //set data for recycler view
        viewModel.makeupList.observe(this, {
            makeupListItems = it
            adapter.setMakeupsList(it)
        })

        //remove progress bar once data has been received.
        viewModel.isLoading.observe(this, {
            if(it == false){
                binding.makeupListProgressBar.visibility = View.GONE
            }
        })

        //display message if error
        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    //pass through clicked makeup object to details activity.
    private fun onListItemClick(position: Int){
        //get makeup object based off of image
        val makeupItem = makeupListItems[position] as GeneralItem
        val makeup: Makeup = makeupItem.getItem()

        val intent = Intent(this, MakeUpDetailsActivity::class.java)
        intent.putExtra("MAKEUP.SELECTED", makeup)
        startActivity(intent)
    }
}