package com.example.absamakeupapp.models

import android.os.Parcel
import android.os.Parcelable

//makeup data class with parcelable implementation for passing through activity.
data class Makeup(
    val id: Int,
    val brand: String?,
    val name: String?,
    val description: String?,
    val category: String?,
    val product_type: String?,
    val product_link: String?,
    val image_link: String?,
    val price_sign: String?,
    val price: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(brand)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(category)
        parcel.writeString(product_type)
        parcel.writeString(product_link)
        parcel.writeString(image_link)
        parcel.writeString(price_sign)
        parcel.writeString(price)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Makeup> {
        override fun createFromParcel(parcel: Parcel): Makeup {
            return Makeup(parcel)
        }

        override fun newArray(size: Int): Array<Makeup?> {
            return arrayOfNulls(size)
        }
    }
}