package com.example.absamakeupapp.models

// list item models to create differentiation between headers and items in recycler view.
abstract class ListItem {
    abstract fun getType(): Int

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_GENERAL = 1
    }
}

class GeneralItem(makeup: Makeup) : ListItem() {
    private val make: Makeup = makeup

    fun getItem(): Makeup {
        return make
    }

    override fun getType(): Int {
        return TYPE_GENERAL
    }
}

class HeaderItem(cat: String) : ListItem(){
    private val category: String = cat

    fun getCategory(): String {
        return category
    }
    override fun getType(): Int {
        return Companion.TYPE_HEADER
    }
}