package com.example.absamakeupapp

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.absamakeupapp.databinding.ActivityMakeUpDetailsBinding
import com.example.absamakeupapp.models.Makeup
import com.example.absamakeupapp.viewmodel.MakeupDetailsViewModel

class MakeUpDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMakeUpDetailsBinding
    private lateinit var viewModel: MakeupDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_up_details)
        binding = ActivityMakeUpDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //set activity title and make back button available.
        val actionbar = supportActionBar
        actionbar!!.title = getString(R.string.actionbar_details_text)
        actionbar.setDisplayHomeAsUpEnabled(true)

        //get parcelable passed through activity.
        val makeup = intent.getParcelableExtra<Makeup>("MAKEUP.SELECTED")
        //create view model, download image.
        viewModel = MakeupDetailsViewModel(makeup, lifecycleScope)
        viewModel.downloadImage()

        //set rest of data to text views
        binding.makeupDetailsNameTextView.text = makeup?.name
        binding.makeupDetailsPriceSignTextView.text = makeup?.price_sign
        binding.makeupDetailsPriceTextView.text = makeup?.price
        binding.makeupDetailsDescriptionTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(makeup?.description, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(makeup?.description)
        }

        //open browser intent on floating action button click.
        binding.makeupDetailsWebFloatingActionButton.setOnClickListener{
            if(makeup?.product_link != null){
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(makeup.product_link))
                startActivity(browserIntent)
            }
        }

        //once image downloaded set image view to bitmap.
        viewModel.imageBitmap.observe(this, {
            if(it == null){
                binding.makeupDetailsImageView.setImageResource(R.drawable.ic_baseline_broken_image_24)
                Toast.makeText(applicationContext, "Could not fetch image.", Toast.LENGTH_LONG).show()
            }
            else{
                binding.makeupDetailsImageView.setImageBitmap(it)
            }
        })

        viewModel.isLoadingImage.observe(this, {
            if(it == false){
                binding.makeupDetailsDownloadImageProgressBar.visibility = View.GONE
            }
        })
    }

    //let action bar back arrow simulates back button press.
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}