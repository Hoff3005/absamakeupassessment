package com.example.absamakeupapp.viewmodel

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.*
import com.example.absamakeupapp.models.Makeup
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.URL

class MakeupDetailsViewModel (private val makeup: Makeup?, private val lifecycleCoroutineScope: LifecycleCoroutineScope): ViewModel() {

    val imageBitmap = MutableLiveData<Bitmap>()
    val isLoadingImage = MutableLiveData(true)

    //download image async
    fun downloadImage(){
        val urlImage = URL(makeup?.image_link)

        val result: Deferred<Bitmap?> = lifecycleCoroutineScope.async(Dispatchers.IO) {
            urlImage.toBitmap()
        }

        lifecycleCoroutineScope.launch(Dispatchers.Main) {
            imageBitmap.postValue(result.await())
            isLoadingImage.postValue(false)
        }
    }

    //convert image url to bitmap.
    private fun URL.toBitmap(): Bitmap?{
        return try {
            BitmapFactory.decodeStream(openStream())
        }catch (e: IOException){
            null
        }
    }
}