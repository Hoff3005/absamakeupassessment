package com.example.absamakeupapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.absamakeupapp.models.GeneralItem
import com.example.absamakeupapp.models.HeaderItem
import com.example.absamakeupapp.models.ListItem
import com.example.absamakeupapp.models.Makeup
import com.example.absamakeupapp.repo.ICallback
import com.example.absamakeupapp.repo.MakeupRepository

class MakeupViewModel(private val makeupRepository: MakeupRepository): ViewModel() {

    val makeupList = MutableLiveData<List<ListItem>>()
    val errorMessage = MutableLiveData<String>()
    val isLoading = MutableLiveData(true)

    //get all makeup from repository, pass through interface callback.
    fun getAllMakeUps(){
        makeupRepository.getAllMakeups(object: ICallback{
            override fun onResponse(list: List<Makeup>?) {
                val processedData = createListItems(list)
                makeupList.postValue(processedData)
                isLoading.postValue(false)
            }
            override fun onFailure(message: String?) {
               errorMessage.postValue(message)
            }
        })
    }

    //group makeup data so that we can group it by product type for headers in recycler view.
    private fun createListItems(list: List<Makeup>?): MutableList<ListItem> {
        val listItems = mutableListOf<ListItem>()
        val groupedData = list?.groupBy { it.brand }

        if (groupedData != null) {
            for (category: String? in groupedData.keys) {
                if(category != null){
                    val invoice = HeaderItem(category)
                    listItems.add(invoice)

                    val makeups = groupedData[category]
                    if(makeups != null){
                        for (makeup in makeups) {
                            val general = GeneralItem(makeup)
                            listItems.add(general)
                        }
                    }
                }
            }
        }
        return listItems
    }
}