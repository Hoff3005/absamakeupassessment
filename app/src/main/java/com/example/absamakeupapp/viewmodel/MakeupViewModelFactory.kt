package com.example.absamakeupapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.absamakeupapp.repo.MakeupRepository
import java.lang.IllegalArgumentException

class MakeupViewModelFactory constructor(private val repository: MakeupRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MakeupViewModel::class.java)){
            MakeupViewModel(this.repository) as T
        } else{
            throw IllegalArgumentException("View model not found.")
        }
    }
}