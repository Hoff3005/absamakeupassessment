package com.example.absamakeupapp.repo

import com.example.absamakeupapp.models.Makeup
import com.example.absamakeupapp.service.MakeupService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MakeupRepository(private val makeupService: MakeupService) {

    //get retrofit call and add request to enqueue, enqueue is async.
    fun getAllMakeups(callback: ICallback) {
        val response = makeupService.getAllMakeup()
        response.enqueue(object: Callback<List<Makeup>> {
            override fun onResponse(call: Call<List<Makeup>>, response: Response<List<Makeup>>) {
                callback.onResponse(response.body())
            }
            override fun onFailure(call: Call<List<Makeup>>, t: Throwable) {
                callback.onFailure(t.message)
            }
        })
    }
}