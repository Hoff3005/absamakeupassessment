package com.example.absamakeupapp.repo

import com.example.absamakeupapp.models.Makeup

interface ICallback {
    fun onResponse(list: List<Makeup>?)
    fun onFailure(message: String?)
}